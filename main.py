import re
import yaml
from datetime import datetime

# Define regular expressions to search for markers
start_marker = re.compile(r'START u0 (.*?)$')
end_marker = re.compile(r'Destroyed ActivityRecord (.*?)$')


# Define a function to parse timestamps in the log lines
def parse_timestamp(timestamp_str):
    return datetime.strptime(timestamp_str, '%m-%d %H:%M:%S.%f')


# store the application data
app_data = {}

with open('logcat_applications.txt', 'r') as f:
    for line in f:
        # Look for start and end markers in each line
        start_match = start_marker.search(line)
        end_match = end_marker.search(line)

        # If a start marker is found, extract the package name and start time
        if start_match:
            package = start_match.group(1)
            start_time_str = line.split()[0] + ' ' + line.split()[1]
            start_time = parse_timestamp(start_time_str)

            # Add the package to the dictionary with its start time as the value
            app_data[package] = {'start_time': start_time}

        # If an end marker is found, extract the package name and end time
        if end_match:
            package = end_match.group(1)
            end_time_str = line.split()[0] + ' ' + line.split()[1]
            end_time = parse_timestamp(end_time_str)

            # Calculate the lifespan of the application and add it to the dictionary
            lifespan = (end_time - app_data[package]['start_time']).total_seconds()
            app_data[package]['end_time'] = end_time
            app_data[package]['lifespan'] = lifespan

# Output the dictionary to a YAML file
with open('output.yml', 'w') as f:
    yaml.dump(app_data, f)

# Count the number of applications found
num_apps = len(app_data)
print(f'{num_apps} applications found')
