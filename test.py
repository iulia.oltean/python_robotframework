import re
import yaml
from datetime import datetime

# Define regular expressions to search for markers
start_marker = re.compile(r'START u0 (.*?)$')
end_marker = re.compile(r'Destroyed ActivityRecord (.*?)$')

# Define a function to parse timestamps in the log lines
def parse_timestamp(timestamp_str):
    return datetime.strptime(timestamp_str, '%m-%d %H:%M:%S.%f')

# Store the application data in a list
app_data = []

with open('logcat_applications.txt', 'r') as f:
    for line in f:
        # Look for start and end markers in each line
        start_match = start_marker.search(line)
        end_match = end_marker.search(line)

        # If a start marker is found, extract the package name and start time
        if start_match:
            package = start_match.group(1)
            start_time_str = line.split()[0] + ' ' + line.split()[1]
            start_time = parse_timestamp(start_time_str)

            # Add the package data to the list
            app_data.append({
                'app_path': package,
                'ts_app_started': str(start_time),
                'ts_app_closed': '',
                'lifespan': ''
            })

        # If an end marker is found, extract the package name and end time
        if end_match:
            package = end_match.group(1)
            end_time_str = line.split()[0] + ' ' + line.split()[1]
            end_time = parse_timestamp(end_time_str)

            # Find the corresponding package in the list and update its data
            for package_data in app_data:
                if package_data['app_path'] == package:
                    package_data['ts_app_closed'] = str(end_time)
                    package_data['lifespan'] = str(end_time - datetime.strptime(package_data['ts_app_started'], '%Y-%m-%d %H:%M:%S.%f'))

with open('output.yml', 'w') as f:
    yaml.dump(app_data, f)

num_apps = len(app_data)
print(f'{num_apps} applications found')
