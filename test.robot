*** Settings ***
Library    Process
Library    Collections
Library    OperatingSystem

*** Test Cases ***
Test Application Lifespan
    [Documentation]    Test the lifespan of applications in the log file
    [Tags]    lifespan
    ${log_file}=    Get File    logcat_applications.txt
    ${output}=    Run Process    python    lifespan_parser.py    ${log_file}
    ${data}=    Evaluate    yaml.load('''${output}''')
    ${total_apps}=    Get Length    ${data}
    ${long_apps}=    Create List
    ${short_apps}=    Create List
    :FOR    ${key}    IN    @{data.keys()}
        ${lifespan}=    ${data}[${key}]['lifespan']
        Run Keyword If    ${lifespan} > 30
            Append To List    ${long_apps}    ${key}
        ...    ELSE
            Append To List    ${short_apps}    ${key}
    ${num_long_apps}=    Get Length    ${long_apps}
    ${num_short_apps}=    Get Length    ${short_apps}
    ${percent_short_apps}=    Set Variable If    ${num_short_apps} > 0
        ...    ${num_short_apps} / ${total_apps} * 100.0
        ...    0
    ${percent_long_apps}=    Set Variable If    ${num_long_apps} > 0
        ...    ${num_long_apps} / ${total_apps} * 100.0
        ...    0
    Log    Total Applications: ${total_apps}
    Log    Short-lived Applications: ${num_short_apps} (${percent_short_apps}%)
    Run Keyword If    ${num_long_apps} > 0
        Log Many    WARN: Long-lived Applications (>30s):    @{long_apps}
    Log    Long-lived Applications: ${num_long_apps} (${percent_long_apps}%)
    Run Keyword If    ${percent_short_apps} >= 75
        Log    Test PASSED
    ...    ELSE
        Log    Test FAILED
